package com.example.largo.elixirapp;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.example.elixir.R;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.StatementBuilder;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.Time;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class MainActivity extends AppCompatActivity {
    private TimePicker timePicker;
    //private TextView textViewTime;
    private int hour;
    private int minute;
    private int currentHour;
    private int currentMinute;
    private String value1;
    private String value2;
    private String przychodzacy;
    private String wychodzacy;
    protected String timeTransfer;
    private String godzina;
    //static final int TIME_DIALOG_ID = 999;
    // URL Address
    //String url = "http://www.androidbegin.com";
    ProgressDialog mProgressDialog;
    Dao<Banks, ?> dao;
    private HashMap<String, String> banks;
    private Toolbar toolbar;
    private BootReceiver receiver;


    private IntentFilter filter =
            new IntentFilter(Intent.ACTION_TIME_TICK);

    private BroadcastReceiver broadcast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            onTimeChanged();
        }
    };

    private void onTimeChanged() {

        DateFormat df = new SimpleDateFormat("kk:mm", Locale.UK);
        String time = df.format(Calendar.getInstance().getTime());

        Toast.makeText(getApplicationContext(), time, Toast.LENGTH_LONG).show();

        if (godzina != null && value2 != null) {
            if (godzina.equals(time)) {
                createNotification();
            }
        }

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setCurrentTimeOnView();

        // Locate the Buttons in activity_main.xml
        Button getLinks = (Button) findViewById(R.id.button1);

        Spinner spinner = (Spinner) findViewById(R.id.spinner1);
        Spinner spinner2 = (Spinner) findViewById(R.id.spinner2);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.banki, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner2.setAdapter(adapter);

        DBHelper dbHelper = new DBHelper(this);

        if (checkInternetConnection() == true) {
            Log.e(getClass().getSimpleName(), "SUCCESS");

            try {
                dao = dbHelper.getDao(Banks.class);
                //dao.executeRaw("drop table banki;");

            /*List<Banks> result = dao.queryForAll();

            for (Banks bank: result) {
                System.out.println(bank.nazwa);
            }*/

            } catch (SQLException e) {
                e.printStackTrace();
            }

            try {
                List<Banks> result = dao.queryForAll();

                for (Banks bank : result) {
                    System.out.println(bank.nazwa);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }


            // Capture Links
            getLinks.setOnClickListener(new OnClickListener() {
                public void onClick(View arg0) {
                    value1 = ((Spinner) findViewById(R.id.spinner1)).getSelectedItem().toString();
                    value2 = ((Spinner) findViewById(R.id.spinner2)).getSelectedItem().toString();
                    if (value1.equals(value2)) {
                        pokazDialog(getString(R.string.przelew_wewnetrzny));
                    } else {
                        // Execute Links AsyncTask
                        getTimeFromPicker();
                        new Links().execute();
                    }
                }
            });
        }
        else {

            Toast.makeText(
                    getApplicationContext(), "Brak połaczenia z internetem", Toast.LENGTH_LONG)
                    .show();
            try {
                dao = dbHelper.getDao(Banks.class);
                //dao.executeRaw("drop table banki;");

            List<Banks> result = dao.queryForAll();

            for (Banks bank: result) {
                System.out.println(bank.nazwa);
            }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            // Capture Links
            getLinks.setOnClickListener(new OnClickListener() {
                public void onClick(View arg0) {
                    value1 = ((Spinner) findViewById(R.id.spinner1)).getSelectedItem().toString();
                    value2 = ((Spinner) findViewById(R.id.spinner2)).getSelectedItem().toString();
                    if (value1.equals(value2)) {
                        pokazDialog(getString(R.string.przelew_wewnetrzny));
                    } else {
                        // Execute Links AsyncTask
                        getTimeFromPicker();

                        try {
                            List<Banks> bank_wy = dao.queryBuilder().where().eq("nazwa", value1).query();
                            List<Banks> bank_wch = dao.queryBuilder().where().eq("nazwa", value2).query();

                            for (Banks bank : bank_wy) {
                                wychodzacy = bank.sesja_wychodzaca;
                            }
                            for (Banks bank : bank_wch) {
                                przychodzacy = bank.sesja_przychodzaca;
                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }

                        new LoadFromBase().execute();
                    }
                }
            });
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcast, filter);

    }

    @Override
    protected void onPause() {
        unregisterReceiver(broadcast);
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.update) {
            if (checkInternetConnection()) {
                new Sessions(this).execute();

                Toast.makeText(this, "Dane zostały zaaktualizowane", Toast.LENGTH_LONG).show();
            }
            else {
                Toast.makeText(
                        getApplicationContext(), "Brak połaczenia z internetem", Toast.LENGTH_LONG)
                        .show();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    // display current time
    public void setCurrentTimeOnView() {
        timePicker = (TimePicker) findViewById(R.id.timePicker1);

        java.util.Formatter timeF = new java.util.Formatter();
        timeF.format("Time defaulted to %d:%02d", timePicker.getCurrentHour(),
                timePicker.getCurrentMinute());

        timePicker.setIs24HourView(true);
        timePicker.setCurrentHour(new Integer(10));
        timePicker.setCurrentMinute(new Integer(10));

        final Calendar c = Calendar.getInstance();
        hour = c.get(Calendar.HOUR_OF_DAY);
        minute = c.get(Calendar.MINUTE);
        // set current time into timepicker
        timePicker.setCurrentHour(hour);
        timePicker.setCurrentMinute(minute);

    }

    public void getTimeFromPicker() {
        currentHour = timePicker.getCurrentHour();
        currentMinute = timePicker.getCurrentMinute();
        timeTransfer = currentHour + ":" + currentMinute;
    }

    public void pokazDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setMessage(msg);
        builder.setCancelable(true);
        builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void getBanks(HashMap<String, String> banks) {
        this.banks = banks;
    }

    private static String padding_str(int c) {
        if (c >= 10)
            return String.valueOf(c);
        else
            return "0" + String.valueOf(c);
    }

    private boolean checkInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        // test for connection
        return cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isAvailable()
                && cm.getActiveNetworkInfo().isConnected();
    }

    public void createNotification() {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle("Nadchodząca sesja")
                        .setContentText("Sesja do banku "+ value2
                                + " jest realizowana ");
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.notify(001, mBuilder.build());
    }

    private class Links extends AsyncTask<Void, ArrayList<String>, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(MainActivity.this);
            mProgressDialog.setTitle("Wczytywanie sesji Elixir");
            mProgressDialog.setMessage("Czekaj...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }


        @Override
        protected Void doInBackground(Void... params) {


            HTMLParser ht = new HTMLParser(value1, value2);

            przychodzacy = ht.sesjaPrzychodzaca();
            wychodzacy = ht.sesjaWychodzaca();
            try {
                godzina = ht.potnijIoblicz(wychodzacy, przychodzacy, timeTransfer);
            } catch (java.text.ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            /*TextView textview = (TextView) findViewById(R.id.textView2);

            textview.setText(godzina);*/

            pokazDialog(godzina);
            // Set title into TextView
            /*String przychodzacySplit, wychodzacySplit;

			Spinner spinnerWy = (Spinner)findViewById(R.id.spinner1);
			String bankWy = spinnerWy.getSelectedItem().toString();

			Spinner spinnerW = (Spinner)findViewById(R.id.spinner2);
			String bankW = spinnerW.getSelectedItem().toString();



			przychodzacySplit = przychodzacy.replace("[", "").replace("]", "").replace(",", "\n").replace("\n ", "\n");
			wychodzacySplit = wychodzacy.replace("[", "").replace("]", "").replace(",", "\n").replace("\n ", "\n");*/


            mProgressDialog.dismiss();
        }

    }

    public class Sessions extends AsyncTask<Void, ArrayList<String>, Void> {

        private Context context;

        Sessions(Context context) {

            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Toast.makeText(context, "Aktualizowanie danych", Toast.LENGTH_LONG).show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            HashMap<String, List<String>> banks = new HashMap<>();

            //List<String> temp = new ArrayList<String>();
            Document doc;
            try {

                // need http protocol
                doc = Jsoup.connect("http://www.najlepszekonto.pl/sesje-elixir").get();

                // get all links
                Elements links = doc.select("tr");
                for (Element link : links) {
                    List<String> sessionOUT = new ArrayList<String>();
                    List<String> sessionIN = new ArrayList<String>();
                    List<String> sessions = new ArrayList<>();

                    String title = link.select("th").text();
                    //temp.clear();

                    int colIndex = 0;

                    for (Element col : link.select("td")) {
                        if (colIndex < 3)
                            sessionOUT.add(col.text());
                        else
                            sessionIN.add(col.text());
                        colIndex++;
                    }
                    sessions.add(sessionOUT.toString());
                    sessions.add(sessionIN.toString());
                    banks.put(title, sessions);

                }


            } catch (IOException e) {
                e.printStackTrace();
            }

            for (Map.Entry<String, List<String>> entry : banks.entrySet()) {
                try {
                    long records = dao.queryBuilder()
                            .where()
                            .eq(Banks.FIELD_NAME_NAME, entry.getKey())
                            .countOf();

                    if (records == 0)
                        dao.createOrUpdate(Banks.banks(entry.getKey(), entry.getValue().get(0), entry.getValue().get(1)));

                } catch (SQLException e) {
                    e.printStackTrace();
                }
                //Banks bank = new Banks(entry.getKey(), entry.getValue().get(0), entry.getValue().get(1));
                //System.out.println(entry.getKey() + "/" + entry.getValue());
                //System.out.println(entry.getKey() + "/" + entry.getValue().get(0) + "/" + entry.getValue().get(1));
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(context, "Dane zostały zaaktualizowane", Toast.LENGTH_SHORT).show();
        }
    }

    public class LoadFromBase extends AsyncTask{

        @Override
        protected Object doInBackground(Object[] objects) {

            HTMLParser ht = new HTMLParser(value1, value2);

            try {
                Log.e(getClass().getSimpleName(), wychodzacy);

                godzina = ht.potnijIoblicz(wychodzacy, przychodzacy, timeTransfer);
            } catch (java.text.ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            pokazDialog(godzina);
        }
    }

}


