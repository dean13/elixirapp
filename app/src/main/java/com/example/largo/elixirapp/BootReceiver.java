package com.example.largo.elixirapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.PowerManager;
import android.os.Vibrator;
import android.widget.Toast;

import com.example.elixir.R;

/**
 * Created by Largo on 06.06.2017.
 */

public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "Toast Receive", Toast.LENGTH_LONG).show();
    }
}
