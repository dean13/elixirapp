package com.example.largo.elixirapp;

import android.annotation.SuppressLint;
import android.util.Log;

import java.io.IOException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class HTMLParser {

    private final String value1;
    private final String value2;

    public HTMLParser(String value1, String value2) {
        this.value1 = value1;
        this.value2 = value2;
    }

    public String sesjaPrzychodzaca() {
        HashMap<String, String> map = new HashMap<String, String>();
        List<String> title = new ArrayList<String>();
        List<String> dateSend = new ArrayList<String>();
        List<String> temp = new ArrayList<String>();
        Document doc;
        try {

            // need http protocol
            doc = Jsoup.connect("http://www.najlepszekonto.pl/sesje-elixir").get();

            // get all links
            Elements links = doc.select("tr");
            for (Element link : links) {

                String name = link.select("th").text();
                title.add(name);
                temp.clear();

                int index = 0;

                for (Element col : link.select("td")) {
                    if (index < 3) {
                        index++;
                    }
                    else
                        temp.add(col.text());
                    //dict.put(title,col.text());

                }
                dateSend.add(temp.toString());
                //System.out.println("\n");
                //dict.put(name, dateSend);
            }
            /*dateSend.remove(1);
            dateSend.remove(0);*/
            /*for (String i : dateSend){
             System.out.println("\n"+i);
             }*/
            /*title.remove(1);
            title.remove(0);*/

            //scalenie dw�ch list do s�ownika
            if (title.size() == dateSend.size()) {
                for (int index = 0; index < title.size(); index++) {
                    map.put(title.get(index), dateSend.get(index));

                }
                //wypisanie ca�ego s�ownika
                /*for (Map.Entry<String, String> entry : map.entrySet()) {
                 System.out.println(entry.getKey() + "/" + entry.getValue());
                 }*/
                //wyswietlenie sesji dla klucza BNP Paribas

                //System.out.println(map.get(value1.replace(",", "\n")));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return map.get(value2);
    }

    public String sesjaWychodzaca() {
        HashMap<String, String> map = new HashMap<String, String>();
        List<String> title = new ArrayList<String>();
        List<String> dateSend = new ArrayList<String>();
        List<String> temp = new ArrayList<String>();
        Document doc;
        // need http protocol
        try {
            doc = Jsoup.connect("http://www.najlepszekonto.pl/sesje-elixir").get();
            // get all links
            Elements links = doc.select("tr");
            for (Element link : links) {
                String name = link.select("th").text();
                //System.out.println(link.select("td[width=80]").text());
                title.add(name);
                temp.clear();

                int index = 0;

                for (Element col : link.select("td")) {
                    if (index < 3) {
                        temp.add(col.text());
                        index++;
                    }
                    //dict.put(title,col.text());

                }
                dateSend.add(temp.toString());
                //System.out.println("\n");
                //dict.put(name, dateSend);
            }
            /*dateSend.remove(1);
            dateSend.remove(0);*/
            /*for (String i : dateSend){
             System.out.println("\n"+i);
             }*/
            /*title.remove(1);
            title.remove(0);*/

            //scalenie dw�ch list do s�ownika
            if (title.size() == dateSend.size()) {
                for (int index = 0; index < title.size(); index++) {
                    map.put(title.get(index), dateSend.get(index));

                }
                //wypisanie ca�ego s�ownika
                /*for (Map.Entry<String, String> entry : map.entrySet())
                 {
                 System.out.println(entry.getKey() + "/" + entry.getValue());
                 }*/
                //wyswietlenie sesji dla klucza jako parametr

                //System.out.println(map.get(value2.replace(",", "\n")));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return map.get(value1);

    }

    @SuppressWarnings("unused")
	@SuppressLint("SimpleDateFormat") public String potnijIoblicz(String czasWy, String czasW, String przelew) throws ParseException {

    	String przelewP = "";
        String przelewD = "Przelew do godziny:";
        String kolejnyD = "Przelew kolejnego dnia:";
        boolean dzisiaj = true;

        Log.e(getClass().getSimpleName(), czasWy +"---"+czasW);
        String replaceCzasWy = czasW.replace("[", "").replace("]", "");
        String replaceOd = czasWy.replace("[", "").replace("]", "");
        try {
            replaceCzasWy.replace(" ", "").replace("od", "").replace("do", "");
            replaceOd.replace(" ", "").replace("od", "").replace("do", "");
        } catch (NullPointerException e) {

        }

        String[] pocietyczasWyD = new String[3];
        String[][] sesjeWyDl = new String[3][2];
        String[] pocietyCzas = new String[30];
        String[] pocietyCzasWy = new String[30];
        String[] pocietyCzasTwice = new String[6];
        String[] pocietyCzasWyTwice = new String[6];

        SimpleDateFormat dateFormat1 = new SimpleDateFormat("HH:mm");
        Date[] sesjeWej = new Date[7];
        Date[] sesjeWyj = new Date[7];

        if (replaceCzasWy.length() > 70) {
            pocietyczasWyD = replaceCzasWy.split(",");
            pocietyCzas = replaceOd.split(",");

            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 1; j++) {
                    sesjeWyDl[i][j] = pocietyczasWyD[i].substring(0, 5);
                }
                for (int n = 1; n < 2; n++) {
                    if (pocietyczasWyD[i] != null) {
                        System.err.println(pocietyczasWyD[i]);
                        sesjeWyDl[i][n] = pocietyczasWyD[i];
                        //sesjeWyDl[i][n] = pocietyczasWyD[i].substring(22, 27);
                    }
                }
            }
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
            Date przeleww = dateFormat.parse(przelew);
            Date[][] sesjeW = new Date[3][2];

            for (int i = 0; i < 3; i++) {
                sesjeWej[i] = dateFormat.parse(pocietyCzas[i]);

                for (int j = 0; j < 2; j++) {
                    sesjeW[i][j] = dateFormat.parse(sesjeWyDl[i][j]);
                }
            }
            for (int x = 0; x < 3; x++) {
                for (int y = 1; y < 2; y++) {
                    if (przeleww.before(sesjeW[x][y]) && sesjeW[x][y].before(sesjeWej[x])) {
//                        System.out.println("Przelew zlecony do godziny: " + sesjeW[x][y] + " Sesja o: " + sesjeW[x][y - 1]);
//                        System.out.println("Przelew przyjdzie o godzinie: " + sesjeWej[x]);
                        dzisiaj=true;
                          przelewP = sesjeWej[x].toString();
                        break;
                    } else if (przeleww.before(sesjeW[x + 1][y]) && sesjeW[x + 1][y - 1].before(sesjeWej[x + 1])) {
//                        System.out.println("Przelew zlecony do godziny: " + sesjeW[x + 1][y] + " Sesja o: " + sesjeW[x + 1][y - 1]);
//                        System.out.println("Przelew przyjdzie o godzinie: " + sesjeWej[x + 1]);
                        dzisiaj=true;
                          przelewP = sesjeWej[x+1].toString();
                        break;
                    } else if (przeleww.before(sesjeW[x + 2][y]) && sesjeW[x + 2][y - 1].before(sesjeWej[x + 2])) {
//                        System.out.println("Przelew zlecony do godziny: " + sesjeW[x + 2][y] + " Sesja o: " + sesjeW[x + 2][y - 1]);
//                        System.out.println("Przelew przyjdzie o godzinie: " + sesjeWej[x + 2]);
                        dzisiaj = true;
                          przelewP = sesjeWej[x+2].toString();
                        break;
                    } else {
                        dzisiaj = false;
//                        System.out.println("Przelew kolejnego dnia");
                          przelewP = sesjeW[0][0].toString();
                    }
                }
                break;
            }

        } else {
            pocietyCzasTwice = replaceOd.split("[,-]");
            pocietyCzasWyTwice = replaceCzasWy.split("[,-]");
            pocietyCzas = replaceOd.split(",");
            pocietyCzasWy = replaceCzasWy.split(",");

//sprawdza czy sesja nie jest z zakresu od-do
            if (pocietyCzas[0].length() > 5 || pocietyCzasWy[0].length() > 5) {  
                for (int i = 0; i < pocietyCzasWyTwice.length; i++) {
                    sesjeWej[i] = dateFormat1.parse(pocietyCzasWyTwice[i]);
//                    System.out.println(sesjeWej[i]);
                }
                for (int i = 0; i < pocietyCzasTwice.length; i++) {
                    sesjeWyj[i] = dateFormat1.parse(pocietyCzasTwice[i]);
                }
                Date przeleww = dateFormat1.parse(przelew);
                
                for (int i = 0; i < sesjeWyj.length; i++) {
                    if (przeleww.before(sesjeWyj[i]) || przeleww.before(sesjeWyj[i+1])) {
//                        System.out.println("Przelew wyjdzie o godzinie: " + sesjeWyj[i+1]);
//                        przelewW = przelewW + sesjeWyj[i+1];
                        for (int j = 0; j < sesjeWej.length; j++) {
                            if (sesjeWyj[i+1].before(sesjeWej[j])) {
//                                System.out.println("Przelew przyjdzie o godzinie: " + sesjeWej[j]);
                                dzisiaj = true;
                                przelewP = sesjeWej[j].toString();
                                break;
                            }       
                            }

                        break;
                    }
                    else if (przeleww.after(sesjeWyj[2])) {
                        dzisiaj = false;
//                        System.out.println("Przelew kolejnego dnia o godzinie: " + sesjeWej[0]);
                        przelewP = sesjeWej[0].toString();
                        break;
                    }
                }
            }
//je�li nie jest to leci dalej
            for (int i = 0; i < 3; i++) {
                sesjeWyj[i] = dateFormat1.parse(pocietyCzas[i]);
            }
            for (int i = 0; i < 3; i++) {
                sesjeWej[i] = dateFormat1.parse(pocietyCzasWy[i]);
            }

            Date przeleww = dateFormat1.parse(przelew);
//            System.out.println(sesjeWej[0]+" "+sesjeWyj[0]);
            for (Date i : sesjeWyj) {
                if (przeleww.before(i) || przeleww.equals(i)) {
//                    System.out.println("Przelew wyjdzie o godzinie: " + i);
//                    przelewP = i.toString();
                    for (Date j : sesjeWej) {
//                        System.out.println(j);
                        if (j == null) {
                            dzisiaj = false;
                            przelewP = sesjeWej[0].toString();
                        }
                        else if (i.before(j)) {
//                            System.out.println("Przelew wjdzie o godzinie: " + j);
                            dzisiaj = true;
                            przelewP = j.toString();
                            break;
                        }
                        else {
                            dzisiaj = true;
                            przelewP = sesjeWej[0].toString();
                        }
                        
                    }
                    break;
                } 
                else if (przeleww.after(sesjeWyj[2]) || przeleww.equals(sesjeWyj[2])) {
//                    System.out.println("Przelew kolejnego dnia o godzinie: " + sesjeWyj[0]);
                    dzisiaj = false;
                    przelewP = sesjeWej[0].toString();
                    break;
                } 
                else if (przeleww.equals(i) && przeleww.before(i)) {
//                    System.out.println("Przelew wyjdzie o godzinie: " + i + 1);
                    przelewP = i.toString() + 1;
                } 
            }
        }
        if (dzisiaj==true){
            return przelewD+przelewP.substring(10, 16);
        }
        else 
            return kolejnyD+przelewP.substring(10, 16);
    }

}
