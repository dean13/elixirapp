package com.example.largo.elixirapp;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Largo on 27.04.2017.
 */

@DatabaseTable(tableName = "banki")
public class Banks {

    public static final String FIELD_NAME_NAME = "nazwa";
    public static final String FIELD_NAME_ENTER = "sesja_przychodzaca";
    public static final String FIELD_NAME_EXIT = "sesja_wychodzaca";

    @DatabaseField(generatedId = true)
    public long id;

    @DatabaseField(columnName = FIELD_NAME_NAME)
    public String nazwa;

    @DatabaseField
    public String sesja_przychodzaca;

    @DatabaseField
    public String sesja_wychodzaca;

    public Banks() {
    }

    public static Banks banks(String nazwa, String sesja_przychodzaca, String sesja_wychodzaca) {
        Banks banks = new Banks();
        banks.nazwa = nazwa;
        banks.sesja_przychodzaca = sesja_przychodzaca;
        banks.sesja_wychodzaca = sesja_wychodzaca;

        return banks;
    }
}
